# Crawly

Collects all links found from a starting url that share the same host. Crawly doesn't read and therefore doesn't act on the robots.txt file of the targeted website. 

## Usage

Example usage:

```
$ mkdir -p $GOPATH/src/gitlab.com/eikespang && cd $_
$ git clone git@gitlab.com:eikespang/crawly.git
$ cd crawly
$ go build
$ ./crawly --help
Usage of ./crawly:
  -n uint
    	number of goroutines fetching urls at any given time (default 100)
  -o string
    	name of the output file (default "out.dot")
  -url string
    	The url crawly starts with. It will follow all links of the host of this url
$ ./crawly -url http://www.example.com -n 100
```

The crawler will start n go-routines to fetch all links that are found when starting from the given url not visiting any url twice.

## Visualise site map

Install Graphviz. Crawly will return a dot file which can be used to visualise the site map:

```
$ dot -Tps out.dot -o outfile.ps
```

and open outfile.ps.

