package crawl

import (
	"net/http"
	"sync/atomic"
	"time"

	"github.com/sirupsen/logrus"
)

// Config is used to configure
// the Crawler.
type Config struct {
	StartingURL string
	N           uint
	Logger      *logrus.Logger
	Timeout     time.Duration
}

// Edge is a link from one url to another.
type Edge struct {
	From string
	To   string
}

// Crawl crawls a host as configured
func Crawl(cfg Config) []Edge {
	fetched := map[string]struct{}{}
	edges := map[string]map[string]struct{}{}

	// I started with buffersizes of 250-2500
	// but when crawling "https://golang.org"
	// this channels fill up fast and the crawler deadlocks.
	// Those values are big enough to crawl golang.org.
	// A better solution would be to keep a independend
	// buffer of the urls that should be fetched and use
	// the channels just for communication. This would also
	// need additional synchronization primitives and would
	// make the solution more complicated.
	newURLs := make(chan Edge, 100000)
	urlsToFetch := make(chan string, 100000)

	// counter to count inflight fetches. This is used
	// to check if we are done.
	// Done means the toFetch set is empty and inflight
	// is 0.
	var inflight int64

	// Start workers that fetch urls given by urlsToFetch
	for i := uint(0); i < cfg.N; i++ {
		go func() {
			fetcher := &fetcher{
				client: &http.Client{Timeout: cfg.Timeout},
				logger: cfg.Logger,
			}

			for url := range urlsToFetch {
				atomic.AddInt64(&inflight, 1)

				cfg.Logger.WithField("url", url).Info("fetching url")
				urls, err := fetcher.Fetch(url)
				if err != nil {
					cfg.Logger.WithError(err).WithField("url", url).Warn("failed to fetch a url")
				} else {
					sendAll(url, urls, newURLs)
				}

				atomic.AddInt64(&inflight, -1)
			}
		}()
	}
	fetched[cfg.StartingURL] = struct{}{}
	urlsToFetch <- cfg.StartingURL

	// Consume new URLs
LOOP:
	for {
		select {
		case edge := <-newURLs:
			newURL := edge.To
			if _, ok := edges[edge.From]; !ok {
				edges[edge.From] = map[string]struct{}{}
			}
			edges[edge.From][edge.To] = struct{}{}

			if _, ok := fetched[newURL]; ok {
				// this url has been seen. Don't handle it anymore
				continue
			}

			cfg.Logger.WithField("url", newURL).Info("enqueue url to be fetched")
			fetched[newURL] = struct{}{}
			urlsToFetch <- newURL
		case <-time.After(cfg.Timeout):
			if atomic.LoadInt64(&inflight) == 0 {
				close(urlsToFetch)
				cfg.Logger.Info("infliht is 0; stop")
				break LOOP
			}
		}

	}

	var ret []Edge
	for from, s := range edges {
		for to := range s {
			ret = append(ret, Edge{From: from, To: to})
		}
	}
	return ret
}

func sendAll(from string, urls []string, recv chan<- Edge) {
	for _, url := range urls {
		recv <- Edge{From: from, To: url}
	}
}
