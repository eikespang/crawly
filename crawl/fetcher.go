package crawl

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"github.com/sirupsen/logrus"
	"golang.org/x/net/html"
)

type fetcher struct {
	client *http.Client
	logger *logrus.Logger
}

// Fetches all links at rawURL that are at the same host.
func (f *fetcher) Fetch(rawURL string) ([]string, error) {
	u, err := url.Parse(rawURL)
	if err != nil {
		return nil, err
	}

	resp, err := f.client.Get(rawURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%q returned non 200 response %s", rawURL, resp.Status)
	}

	return extractLinks(f.logger, u.Host, u.Scheme, resp.Body)
}

func extractLinks(logger *logrus.Logger, host, scheme string, body io.Reader) ([]string, error) {
	tokenizer := html.NewTokenizer(body)

	var urls []string
	for {
		tokenType := tokenizer.Next()
		if tokenType == html.ErrorToken {
			if err := tokenizer.Err(); err != io.EOF {
				return nil, err
			}
			return urls, nil
		}

		if tokenType == html.StartTagToken {
			token := tokenizer.Token()

			if isLink(token) {
				if rawURL, ok := extractLink(token); ok {
					u, err := url.Parse(rawURL)
					if err != nil {
						logger.WithError(err).WithField("raw_url", rawURL).Warn("couldn't parse url")
						continue
					}
					if u.Host == "" {
						u.Host = host
					}
					if u.Host != host {
						continue
					}
					if u.Scheme == "" {
						u.Scheme = scheme
					}
					urls = append(urls, urlToString(u))
				}
			}
		}
	}
}

func isLink(token html.Token) bool {
	return token.Data == "a"
}

func extractLink(token html.Token) (string, bool) {
	for _, a := range token.Attr {
		if a.Key == "href" {
			if !strings.HasPrefix(a.Val, "mailto") {
				return a.Val, true
			}
		}
	}
	return "", false
}

func urlToString(u *url.URL) string {
	rawURL := u.String()
	if u.Fragment != "" {
		splittedURL := strings.Split(rawURL, "#")
		return splittedURL[0]
	}
	return rawURL
}
