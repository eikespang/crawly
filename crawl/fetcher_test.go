package crawl

import (
	"os"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestExtractLinks(t *testing.T) {
	testcases := []struct {
		label    string
		filename string
		host     string
		scheme   string
		results  []string
		err      error
	}{
		{
			label:    "example.com",
			filename: "example.com",
			host:     "www.example.com",
			scheme:   "http",
			results:  nil,
			err:      nil,
		},
		{
			label:    "google.co.uk",
			filename: "google.co.uk",
			host:     "www.google.co.uk",
			scheme:   "https",
			results: []string{
				"http://www.google.co.uk/imghp?hl=en&tab=wi",
				"https://www.google.co.uk/intl/en/about/products?tab=wh",
				"http://www.google.co.uk/history/optout?hl=en",
				"https://www.google.co.uk/preferences?hl=en",
				"https://www.google.co.uk/advanced_search?hl=en-GB&authuser=0",
				"https://www.google.co.uk/language_tools?hl=en-GB&authuser=0",
				"https://www.google.co.uk/intl/en/ads/",
				"https://www.google.co.uk/services/",
				"https://www.google.co.uk/intl/en/about.html",
				"http://www.google.co.uk/setprefdomain?prefdom=US&sig=K_ejFpE22VrLNQHlmTa2gpop2GDOs%3D",
				"https://www.google.co.uk/intl/en/policies/privacy/",
				"https://www.google.co.uk/intl/en/policies/terms/",
			},
			err: nil,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.label, func(t *testing.T) {
			f, err := os.Open(tc.filename)
			assert.Nil(t, err)

			urls, err := extractLinks(logrus.New(), tc.host, tc.scheme, f)
			assert.Nil(t, err)
			assert.Equal(t, tc.results, urls)
		})
	}
}
