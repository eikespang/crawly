package main

import (
	"flag"
	"os"
	"time"

	memmap "github.com/bradleyjkemp/memviz"
	"github.com/sirupsen/logrus"

	"gitlab.com/eikespang/crawly/crawl"
)

func main() {
	var (
		target      = flag.String("url", "", "The url crawly starts with. It will follow all links of the host of this url")
		output      = flag.String("o", "out.dot", "name of the output file")
		concurrency = flag.Uint("n", 100, "number of goroutines fetching urls at any given time")
	)

	flag.Parse()

	if *target == "" {
		flag.Usage()
		os.Exit(1)
	}

	logger := &logrus.Logger{
		Out: os.Stdout,
		Formatter: &logrus.TextFormatter{
			ForceColors:     true,
			FullTimestamp:   true,
			TimestampFormat: time.StampMilli,
		},
		Level: logrus.DebugLevel,
	}

	config := crawl.Config{
		N:           *concurrency,
		StartingURL: *target,
		Logger:      logger,
		Timeout:     time.Second,
	}

	result := crawl.Crawl(config)
	logger.Infof("Crawling %q returned %d edges", *target, len(result))

	// Transform the list of edges into a graph for visualisation.
	graph := map[string]*Node{}
	for _, edge := range result {
		if _, ok := graph[edge.To]; !ok {
			graph[edge.To] = &Node{
				URL: edge.To,
			}
		}
		if _, ok := graph[edge.From]; !ok {
			graph[edge.From] = &Node{
				URL: edge.From,
			}
		}

		graph[edge.From].LinksTo = append(graph[edge.From].LinksTo, graph[edge.To])
	}

	f, err := os.Create(*output)
	if err != nil {
		logger.WithError(err).Fatal("creating file to write result")
	}

	memmap.Map(f, &graph)
	logger.WithField("output_file", *output).Info("wrote results into file")
}

// Node of a graph containing a name
// and directed edges to other nodes.
type Node struct {
	URL     string
	LinksTo []*Node
}
